def create(value,i,type):
    page = factory()
    words = value.split()
    findMax(len(words))
    page.setList(words)
    if type is 'p':
        page.setName("P" + str(i + 1))
        add_to_global(words, page.getName())
        global_list_of_pages[page.getName()] = page
    else:
        page.setName("Q" + str(i + 1))
        global_list_of_queries.append(page)

def findMax(number):
    global max_weight
    if max_weight < number:
        max_weight = number
